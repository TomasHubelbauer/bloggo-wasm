# WebAssembly

- [ ] Verify Node 8.100 LTS is able to import WASM files without a flag as its supposed to

## Some cool links

- [WebAssembly](http://webassembly.org/)
- [WebAssembly (MDN)](https://developer.mozilla.org/en-US/docs/WebAssembly)
- [Wasm by hand](https://github.com/rhmoller/wasm-by-hand)
- [Wasm game by hand](https://github.com/abagames/wasm-game-by-hand)
- [WABT: The WebAssembly Binary Toolkit](https://github.com/WebAssembly/wabt)

## WebAssembly Studio <3

- https://news.ycombinator.com/item?id=16811721
- https://hacks.mozilla.org/2018/04/sneak-peek-at-webassembly-studio/
- https://webassembly.studio/
- https://mbebenita.github.io/WasmExplorer/
- https://wasdk.github.io/WasmFiddle/

## WebAssembly + Rust

- [`wasm-bindgen`](https://github.com/alexcrichton/wasm-bindgen)
- [`wasm-pack`](https://github.com/ashleygwilliams/wasm-pack)

[Making WebAssembly better for Rust & for all languages by Lin Clark](https://hacks.mozilla.org/2018/03/making-webassembly-better-for-rust-for-all-languages/)

## Cervus

https://github.com/cervus-v/cervus

## Nebulet

https://github.com/nebulet/nebulet
